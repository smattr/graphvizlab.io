---
layout: page
title: Contact
permalink: /contact/
order: 10
---

Please join the <a href="https://forum.graphviz.org" target="_blank">Graphviz forum</a> to ask questions and discuss Graphviz.

If you have a bug or believe something is not working as expected, please 
submit a bug report using the [issues section](https://gitlab.com/graphviz/graphviz/issues) in GitLab.

If you have a general question or are unsure how things work, please consider 
posting to the <a href="https://forum.graphviz.org" target="_blank">Graphviz forum</a>.

[John Ellson](mailto:ellson@graphviz.org) (for problems with builds, installation, software configuration or this web server)

[Emden Gansner](mailto:erg@alum.mit.edu) (for all graph layout issues)

